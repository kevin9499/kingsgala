/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import {MaterialCommunityIcons} from "@expo/vector-icons";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {
    NavigationContainer,
    DefaultTheme,
    DarkTheme,
} from "@react-navigation/native";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import * as React from "react";
import {ColorSchemeName} from "react-native";

import Colors from "../constants/Colors";
import {UserContext} from "../contexts/UserContext";
import useColorScheme from "../hooks/useColorScheme";
import ArticleScreen from "../screens/ArticleScreen";
import InscriptionScreen from "../screens/InscriptionScreen";
import LoginScreen from "../screens/LoginScreen";
import MainScreen from "../screens/MainScreen";
import HomeScreen from "../screens/HomeScreen";
import CalendarScreen from "../screens/CalendarScreen";
import MessageScreen from "../screens/MessageScreen";
import NotFoundScreen from "../screens/NotFoundScreen";
import {RootStackParamList, RootTabParamList} from "../types";
import LinkingConfiguration from "./LinkingConfiguration";
import {GlobalContextProvider} from "../contexts/GlobalContext";
import ForgotPasswordScreen from "../screens/ForgotPasswordScreen";
import ChangePasswordScreen from "../screens/ChangePasswordScreen";
import CartScreen from "../screens/CartScreen";
import ProfilScreen from "../screens/ProfilScreen";
import SupportScreen from "../screens/SupportScreen";

import CastleScreen from "../screens/CastleScreen";

export default function Navigation({
                                       colorScheme,
                                   }: {
    colorScheme: ColorSchemeName;
}) {
    return (
        <NavigationContainer
            linking={LinkingConfiguration}
            theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
        >
            <GlobalContextProvider>
                <RootNavigator/>
            </GlobalContextProvider>
        </NavigationContainer>
    );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
    const {token} = React.useContext(UserContext);
    if (!token) {
        return (
            <Stack.Navigator>
                <Stack.Screen
                    name="Main"
                    component={MainScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Cart"
                    component={CartScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Article"
                    component={ArticleScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Inscription"
                    component={InscriptionScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Forgot"
                    component={ForgotPasswordScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Change"
                    component={ChangePasswordScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Profil"
                    component={ProfilScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="NotFound"
                    component={NotFoundScreen}
                    options={{title: "Oops!"}}
                />
                <Stack.Screen
                    name="Castle"
                    component={CastleScreen}
                    options={{headerShown: false}}
                />
            </Stack.Navigator>
        );
    } else {
        return (
            <Stack.Navigator>
                <Stack.Screen
                    name="Root"
                    component={BottomTabNavigator}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Article"
                    component={ArticleScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="Castle"
                    component={CastleScreen}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="NotFound"
                    component={NotFoundScreen}
                    options={{title: "Oops!"}}
                />
            </Stack.Navigator>
        );
    }
    /*   <Stack.Screen
           name="Support"
           component={SupportScreen}
           options={{headerShown: false}}
       />*/
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
    const colorScheme = useColorScheme();

    return (
        <BottomTab.Navigator
            initialRouteName="Home"
            screenOptions={{
                tabBarActiveTintColor: Colors[colorScheme].tint,
                tabBarShowLabel: false,
                tabBarStyle: {
                    backgroundColor: Colors[colorScheme].background,
                    borderTopWidth: 0,
                    shadowColor: Colors[colorScheme].background,
                },
            }}
        >
            <BottomTab.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color}) => <TabBarIcon name="home" color={color}/>,
                }}
            />
            <BottomTab.Screen
                name="Calendar"
                component={CalendarScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color}) => (
                        <TabBarIcon name="calendar-blank" color={color}/>
                    ),
                }}
            />
            <BottomTab.Screen
                name="Message"
                component={MessageScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color}) => (
                        <TabBarIcon name="message-text" color={color}/>
                    ),
                }}
            />
            <BottomTab.Screen
                name="Cart"
                component={CartScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color}) => <TabBarIcon name="cart" color={color}/>,
                }}
            />
            <BottomTab.Screen
                name="Profil"
                component={ProfilScreen}
                options={{
                    headerShown: false,
                    tabBarIcon: ({color}) => (
                        <TabBarIcon name="account-circle" color={color}/>
                    ),
                }}
            />
        </BottomTab.Navigator>
    );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
    name: React.ComponentProps<typeof MaterialCommunityIcons>["name"];
    color: string;
}) {
    return (
        <MaterialCommunityIcons size={30} style={{marginBottom: -3}} {...props} />
    );
}
