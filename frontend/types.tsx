/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import { BottomTabScreenProps } from "@react-navigation/bottom-tabs";
import {
  CompositeScreenProps,
  NavigatorScreenParams,
} from "@react-navigation/native";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

export type RootStackParamList = {
  Main: undefined;
  Article: undefined;
  Message: undefined;
  Login: undefined;
  Cart: undefined;
  Inscription: undefined;
  Forgot: undefined;
  Change: undefined;
  Support: undefined;
  Root: NavigatorScreenParams<RootTabParamList> | undefined;
  Modal: undefined;
  Profil: undefined;
  NotFound: undefined;
  Home: undefined;
  Castle: { castle: Castle };
};

export type RootStackScreenProps<Screen extends keyof RootStackParamList> =
  NativeStackScreenProps<RootStackParamList, Screen>;

export type Castle = {
  name: string;
  image: any;
  description: string;
  price: number;
  map: any;
  phone: string;
};

export type RootTabParamList = {
  Main: undefined;
  Cart: undefined;
  Article: undefined;
  TabOne: undefined;
  Profile: undefined;
  Home: undefined;
  Calendar: undefined;
  Message: undefined;
  Castle: { castle: Castle };
  Support: undefined;
};

export type RootTabScreenProps<Screen extends keyof RootTabParamList> =
  CompositeScreenProps<
    BottomTabScreenProps<RootTabParamList, Screen>,
    NativeStackScreenProps<RootStackParamList>
  >;
