import {Platform, StyleSheet} from 'react-native';

import {Header, Text, View} from '../components/Themed';
import {RootStackScreenProps} from '../types';
import UserFormRegister from "../components/user/UserFormRegister";

export default function InscriptionScreen({navigation}: RootStackScreenProps<'Inscription'>) {
    return (
        <View style={styles.container}>
            <View style={styles.head}>
                <Header/>
            </View>
            <UserFormRegister redirect={() => navigation.navigate("Login")}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    head: {
        bottom: 50
    }
});
