import { StyleSheet } from "react-native";

import { View } from "../components/Themed";
import { RootStackScreenProps } from "../types";
import UserProfil from "../components/user/UserProfil";

export default function ProfilScreen({
  navigation,
}: RootStackScreenProps<"Profil">) {
  return (
    <View style={styles.container}>
      <UserProfil redirect={() => navigation.navigate("Home")} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  node: {
    fontSize: 40,
  },
});
