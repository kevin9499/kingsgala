import { RootStackScreenProps } from "../types";
import { View } from "../components/Themed";
import { StyleSheet } from "react-native";
import AppHeader from "../components/AppHeader";
import UserMessage from "../components/user/UserMessage";

export default function MessageScreen({
  navigation,
}: RootStackScreenProps<"Message">) {
  return (
    <View style={styles.container}>
      <AppHeader subtitle={"Talk about your bookings"} />
      <UserMessage redirect={() => navigation.navigate("Support")}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
  },
});
