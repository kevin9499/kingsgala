import { RootStackScreenProps } from "../types";
import { View } from "../components/Themed";
import { StyleSheet } from "react-native";
import AppHeader from "../components/AppHeader";

export default function CalendarScreen({
  navigation,
}: RootStackScreenProps<"Change">) {
  return (
    <View style={styles.container}>
      <AppHeader subtitle={"Review your bookings"} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
  },
});
