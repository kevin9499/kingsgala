import { Alert, Image, Platform, StyleSheet, Modal, TouchableHighlight, ActivityIndicator, Pressable } from "react-native";

import { Header, Text, View } from "../components/Themed";
import { RootStackScreenProps } from "../types";
import { UserContext } from "../contexts/UserContext";
import React, { useContext, useEffect, useState } from "react";
// @ts-ignore
import {URL} from "@env";
import { useStripe } from "@stripe/stripe-react-native";

export default function CartScreen({
  navigation,
}: RootStackScreenProps<"Cart">) {
  const { loadCastle, castle } = useContext(UserContext);
  const { profile, setProfile } = useContext(UserContext);
  const [firstname, setFirstname] = useState(profile.firstname);
  const [modalVisible, setModalVisible] = useState(false);
  const [loaderVisible, setLoaderVisible] = useState(false);
  const [loading, setLoading] = useState(false);



  useEffect(() => {
    loadCastle();
  }, []);

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
        <Pressable
              style={[styles.button2, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
            <Text style={styles.textStyle}>x</Text>
                          </Pressable>

          <View style={styles.modalView}>
          <Image
            style={{ width: 200, height: 110, margin: 10, marginTop: 60 }}
            source={require("../assets/images/Stripe-logo.png")}
          />            
          <Text style={styles.modalText}>
          Total :{" "}
          {castle.reduce((total: any, castle: any) => castle.price + total, 0)}
        </Text>
        <View
              style={{
                flexDirection: "row",
                flex: 1,
                maxHeight: 40,
                backgroundColor: "white",
                alignContent: "space-between",
              }}
            >
            <Pressable
              style={[styles.button2, styles.buttonClose]}
              onPress={() => setLoaderVisible(!loaderVisible)}
            >
              <Text style={styles.textStyle}>Pay</Text>
              
            </Pressable>
            <ActivityIndicator animating={loaderVisible} size="large" />
          </View>
          </View>
        </View>
      </Modal>
      <View
        style={{
          flexDirection: "column",
          flex: 1,
          maxHeight: 140,
          alignContent: "space-between",
        }}
      >
        <Text style={styles.title}>Hi {firstname}, Let’s</Text>
        <Text style={styles.subtitle}>Confirm your Bookings</Text>
      </View>
      <View
        style={{
          flexDirection: "column",
          flex: 1,
          maxHeight: 300,
          alignContent: "space-between",
        }}
      >
        {castle.map((c, index) => (
          <View key={index}>
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                maxHeight: 200,
                alignContent: "space-between",
              }}
            >
              <Image
                style={{ width: 100, height: 80, margin: 15, marginTop: 60 }}
                source={require("../assets/images/castlec.png")}
              />
              <View
                style={{
                  flexDirection: "column",
                  alignContent: "space-between",
                }}
              >
                <Text style={styles.title}>{c.name}</Text>
                <Text style={styles.titlecart}>{c.start}</Text>
              </View>
              <Text style={styles.titleprice}> 1 x {c.price} €</Text>
            </View>
            <View
              style={{
                marginTop: 20,
                marginLeft: 25,
                borderBottomColor: "white",
                borderBottomWidth: StyleSheet.hairlineWidth,
              }}
            />
          </View>
        ))}
      </View>
      <View
        style={{
          flexDirection: "row",
          flex: 1,
          position: 'absolute',
          bottom: 0,
          maxHeight: 200,
          alignContent: "space-between",
          paddingTop: 110,
        }}
      >
        
        <Text style={styles.total}>
          Total :{" "}
          {castle.reduce((total: any, castle: any) => castle.price + total, 0)}
        </Text>
        <TouchableHighlight
            style={styles.button}
            onPress={() =>setModalVisible(true)}
          >
            <Text style={styles.buttonText}>Checkout</Text>
          </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "flex-start",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    
    
  },
  title: {
    flex: 1,
    textAlign: "left",
    fontSize: 22,
    fontFamily: "montserrat",
    color: "white",
    marginTop: 65,
    marginLeft: 20,
  },
  total: {
    flex: 1,
    textAlign: "left",
    fontSize: 22,
    fontFamily: "montserrat",
    color: "white",
    marginTop: 40,
    marginLeft: 20,
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
    width: 100,
  },
  titlecart: {
    flex: 1,
    textAlign: "left",
    fontSize: 20,
    fontFamily: "montserrat",
    color: "white",
    marginTop: -10,
    marginLeft: 20,
  },
  modalView: {
    margin: 20,
    minWidth: 400,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button2: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    fontSize: 45,
    textAlign: "center",
    color: "blue"
  },
  titleprice: {
    textAlign: "left",
    fontSize: 20,
    fontFamily: "montserrat",
    color: "white",
    marginTop: 85,
    marginLeft: 20,
  },
  subtitle: {
    flex: 1,
    textAlign: "left",
    fontSize: 20,
    fontFamily: "montserrat",
    color: "gray",
    marginLeft: 20,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#96C4E1",
    padding: 10,
    marginBottom: 8,
    marginRight: 20,
    marginTop: 30,
    width: 150,
    borderRadius: 30,
    justifyContent: "center",
  },
  head: {
    bottom: 130,
  },
});
