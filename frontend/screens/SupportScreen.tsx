import { RootStackScreenProps } from "../types";
import { View } from "../components/Themed";
import { StyleSheet } from "react-native";
import AppHeader from "../components/AppHeader";
import UserSupport from "../components/user/UserSupport";

export default function SupportScreen({
  navigation,
}: RootStackScreenProps<"Support">) {
  return (
    <View style={styles.container}>
      <AppHeader subtitle={"Talk about your bookings"} />
      <UserSupport back={() => navigation.navigate("Home")}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
  },
});
