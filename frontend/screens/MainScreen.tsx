import { StatusBar } from "expo-status-bar";
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  TouchableHighlight,
} from "react-native";
import * as React from "react";
import EditScreenInfo from "../components/EditScreenInfo";
import { Button, Text, View, ViewMontserrat } from "../components/Themed";
import { UserContext } from "../contexts/UserContext";
import { RootStackScreenProps } from "../types";
import { Montserrat } from "../components/StyledText";
import window from "../constants/Layout";

export default function MainScreen({
  navigation,
}: RootStackScreenProps<"Main">) {
  const { token } = React.useContext(UserContext);
  const image = { uri: "../assets/images/login_bg.jpg" };

  return (
    <ViewMontserrat style={styles.container}>
      <ImageBackground
        source={require("../assets/images/login_bg.jpg")}
        resizeMode="cover"
        style={styles.image}
      >
        <View
          style={{
            flexDirection: "row",
            flex: 1,
            maxHeight: 160,
            backgroundColor: "#000000c0",
          }}
        >
          <Image
            style={{ width: 35, height: 20, margin: 20, marginTop: 60 }}
            source={require("../assets/images/logo_white.png")}
          />
          <Text style={styles.title}>KingsGala</Text>
        </View>
        <Text style={styles.text}>
          Space and History To Envision The Future
        </Text>
        <Text style={styles.subtext}>
          Rent castles and other historical spaces for your weddings, seminars
          and other events
        </Text>
        <View
          style={{
            flexDirection: "column",
            flex: 1,
            maxHeight: 160,
            backgroundColor: "#000000c0",
            alignContent: "center",
            alignItems: "center",
          }}
        >
          <TouchableHighlight
            style={styles.button}
            onPress={() => navigation.navigate("Inscription")}
          >
            <Text style={styles.buttonText}>Sign up</Text>
          </TouchableHighlight>
          <Text style={{ color: "#FFFFFF" }}>
            Already have account ?{" "}
            <Text
              style={styles.link}
              onPress={() => navigation.navigate("Login")}
            >
              Log in
            </Text>
          </Text>
        </View>
      </ImageBackground>

      {/* <Text style={styles.title}>Space and History To Envision The Future</Text> */}
      {/* <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      {token?
        <Button onPress={()=>{navigation.navigate("Login")}} text="Login"/>
      : <View/>} */}
    </ViewMontserrat>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#000000c0",
    zIndex: 2,
    justifyContent: "center",
    minWidth: window.window.width,
  },
  image: {
    flex: 1,
    justifyContent: "center",
  },
  title: {
    flex: 1,
    textAlign: "left",
    fontSize: 20,
    fontFamily: "montserrat",
    color: "white",
    marginTop: 57,
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
    width: 100,
  },
  text: {
    color: "white",
    fontSize: 30,
    fontFamily: "montserrat-bold",
    lineHeight: 45,
    paddingTop: 50,
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: "left",
    backgroundColor: "#000000c0",
    flex: 1,
    Width: 350,
  },
  button: {
    alignItems: "center",
    backgroundColor: "#96C4E1",
    padding: 10,
    marginBottom: 8,
    width: 200,
    borderRadius: 30,
    justifyContent: "center",
  },
  link: {
    color: "#96C4E1",
  },
  bottom: {
    alignItems: "center",
  },
  subtext: {
    color: "white",
    fontSize: 16,
    fontFamily: "montserrat",
    lineHeight: 17,
    paddingLeft: 50,
    paddingRight: 100,
    textAlign: "left",
    backgroundColor: "#000000c0",
    flex: 1,
  },
});
