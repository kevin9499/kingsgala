import {Platform, StyleSheet} from 'react-native';

import {Header, Text, View} from '../components/Themed';
import {RootStackScreenProps} from '../types';
import UserFormRegister from "../components/user/UserFormRegister";
import UserForgetPassword from "../components/user/UserForgetPassword";

export default function ForgotPasswordScreen({navigation}: RootStackScreenProps<'Forgot'>) {
    return (
        <View style={styles.container}>
            <View style={styles.head}>
                <Header/>
            </View>
            <UserForgetPassword onSave={() => navigation.navigate("Change")}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    head: {
        bottom: 220
    }
});
