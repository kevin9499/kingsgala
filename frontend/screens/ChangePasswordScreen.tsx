import {StyleSheet} from 'react-native';

import {Header, View} from '../components/Themed';
import {RootStackScreenProps} from '../types';
import UserChangePassword from "../components/user/UserChangePassword";

export default function ChangePasswordScreen({navigation}: RootStackScreenProps<'Change'>) {
    return (
        <View style={styles.container}>
            <View style={styles.head}>
                <Header/>
            </View>
            <UserChangePassword onSave={() => navigation.navigate("Main")}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    head: {
        bottom: 160
    }
});
