import { Castle, RootStackScreenProps } from "../types";
import { View } from "../components/Themed";
import {
  ImageBackground,
  StyleSheet,
  Pressable,
  TextInput,
} from "react-native";
import { Text } from "../components/Themed";
import AppHeader from "../components/AppHeader";
import { SafeAreaView } from "react-native-safe-area-context";
import { ScrollView } from "react-native";
import { castles } from "../assets/jsons/castles";
import { MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";

export default function HomeScreen({
  navigation,
}: RootStackScreenProps<"Change">) {
  return (
    <View style={styles.container}>
      <AppHeader subtitle={"Find your perfect place"} />
      <View style={styles.searchSection}>
        <MaterialIcons
          style={styles.searchIcon}
          name="search"
          size={20}
          color="#7A8888"
        />
        <TextInput
          style={styles.input}
          placeholder="Search"
          placeholderTextColor="#7A8888"
        />
      </View>
      <View style={styles.sliderTitle}>
        <Text style={styles.sliderTextTitle}>Castles</Text>
      </View>
      <View style={styles.castlesContainer}>
        <SafeAreaView style={styles.safeArea}>
          <ScrollView style={styles.scrollView} horizontal={true}>
            {castles.map((castle: Castle, index) => (
              <View key={index} style={styles.card}>
                <ImageBackground
                  style={styles.cardImage}
                  source={castle.image}
                  resizeMode="cover"
                  imageStyle={{ borderRadius: 22 }}
                >
                  <View style={styles.cardView}>
                    <Pressable
                      onPress={() =>
                        navigation.navigate("Castle", { castle: castle })
                      }
                    >
                      <MaterialCommunityIcons
                        name="chevron-right"
                        size={32}
                        color="#fff"
                      />
                    </Pressable>
                  </View>
                </ImageBackground>
              </View>
            ))}
          </ScrollView>
        </SafeAreaView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 30,
    paddingRight: 30,
    flexDirection: "column",
  },
  castlesContainer: {
    flex: 8,
  },
  card: {
    width: 160,
    height: 160,
    borderRadius: 30,
    marginHorizontal: 8,
  },
  scrollView: {},
  safeArea: {},
  cardImage: {
    width: 160,
    height: 160,
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  cardView: {
    position: "absolute",
    backgroundColor: "#191D21CC",
    borderRadius: 8,
    marginLeft: 120,
    marginTop: 8,
  },
  searchSection: {
    marginTop: 30,
    maxHeight: 50,
    minHeight: 50,
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#283636",
    borderRadius: 10,
    width: "100%",
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#283636",
    color: "#191D21CC",
    borderRadius: 10,
  },
  sliderTitle: {
    marginTop: 20,
  },
  sliderTextTitle: {
    fontSize: 30,
    fontWeight: "bold",
  },
});
