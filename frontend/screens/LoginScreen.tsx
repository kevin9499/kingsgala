import { Platform, StyleSheet } from "react-native";

import { Header, Text, View } from "../components/Themed";
import { RootStackScreenProps } from "../types";
import UserFormLogin from "../components/user/UserFormLogin";

export default function LoginScreen({
  navigation,
}: RootStackScreenProps<"Login">) {
  return (
    <View style={styles.container}>
      <View style={styles.head}>
        <Header />
      </View>
      <UserFormLogin
        onSave={() => navigation.navigate("Main")}
        signUp={() => navigation.navigate("Inscription")}
        reset={() => navigation.navigate("Forgot")}
      />
      <View
        style={styles.separator}
        lightColor="#eee"
        darkColor="rgba(255,255,255,0.1)"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  head: {
    bottom: 130,
  },
});
