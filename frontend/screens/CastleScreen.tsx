import { RootStackScreenProps } from "../types";
import { View, Text } from "../components/Themed";
import {
  ImageBackground,
  Pressable,
  StyleSheet,
  Image,
  Linking,
  Share,
  TextInput,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import DropDownPicker from "react-native-dropdown-picker";
import { useState } from "react";
import { TouchableHighlight } from "react-native";

export default function CastleScreen({
  navigation,
  route,
}: RootStackScreenProps<"Castle">) {
  const castle = route.params.castle;
  DropDownPicker.setTheme("DARK");

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: "Wedding", value: "wedding" },
    { label: "Seminar", value: "seminar" },
    { label: "Family event", value: "famevent" },
    { label: "Custom (Chat with us)", value: "custom" },
  ]);

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: route.path ? route.path : castle.name,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log(result.activityType);
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground source={castle.image} style={styles.backgroundImage}>
        <View style={styles.pageTitle}>
          <Text style={styles.pageTitleText}>{castle.name}</Text>
          <Pressable
            style={styles.backBtn}
            onPress={() => navigation.navigate("Home")}
          >
            <MaterialCommunityIcons
              name="chevron-left"
              color="#fff"
              size={26}
            />
            <Text style={styles.backBtnText}>Come Back Home</Text>
          </Pressable>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.description}>{castle.description}</Text>
          <View style={styles.detailsContainer}>
            <View style={styles.castleCard}>
              <Image style={styles.castleCardImage} source={castle.map} />
            </View>
            <View style={styles.castleCard}>
              <Text style={{ fontWeight: "bold" }}>{castle.price} €</Text>
              <Text>/ jours</Text>
            </View>
            <View style={styles.shareContainer}>
              <Pressable
                style={styles.shareCards}
                onPress={() => Linking.openURL(`tel:${castle.phone}`)}
              >
                <MaterialCommunityIcons name="phone" size={32} color="#fff" />
              </Pressable>
              <Pressable style={styles.shareCards} onPress={onShare}>
                <MaterialCommunityIcons
                  name="share-variant"
                  size={32}
                  color="#fff"
                />
              </Pressable>
            </View>
          </View>
          <View style={styles.dropdownEvent}>
            <DropDownPicker
              open={open}
              value={value}
              items={items}
              setOpen={setOpen}
              setValue={setValue}
              setItems={setItems}
            />
            <TextInput
              style={styles.textInput}
              placeholder="Select a Date"
              placeholderTextColor={"#7A8888"}
            />
          </View>
          <View style={{ marginTop: 80 }}>
            <TouchableHighlight
              style={styles.button}
              onPress={() => navigation.navigate("Home")}
            >
              <Text style={styles.buttonText}>Book Now</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    width: "100 %",
    height: "100 %",
    flex: 1,
    justifyContent: "space-between",
  },
  pageTitle: {
    backgroundColor: "transparent",
    marginTop: "20%",
    paddingHorizontal: 30,
  },
  pageTitleText: {
    fontSize: 32,
    fontWeight: "bold",
    textTransform: "uppercase",
  },
  backBtn: {
    marginTop: 10,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "rgba(25, 29,33, 0.5)",
    alignSelf: "flex-start",
    paddingHorizontal: 10,
    borderRadius: 20,
  },
  backBtnText: {
    fontSize: 14,
  },
  infoContainer: {
    width: "100%",
    height: "64%",
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    padding: 30,
  },
  description: {
    fontSize: 18,
  },
  detailsContainer: {
    marginTop: 30,
    width: "100%",
    height: 110,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  castleCard: {
    width: 100,
    height: 100,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#7A8888",
    borderWidth: 1,
    borderRadius: 30,
    marginHorizontal: 10,
  },
  castleCardImage: {
    height: 100,
    width: 100,
    borderRadius: 30,
  },
  shareContainer: {
    width: 70,
    height: 100,
    marginHorizontal: 10,
    display: "flex",
    justifyContent: "space-evenly",
    alignItems: "center",
  },

  shareCards: {
    width: 40,
    height: 40,
    backgroundColor: "#7A8888",
    borderRadius: 10,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  dropdownEvent: {
    marginTop: 10,
  },
  textInput: {
    color: "#fff",
    padding: 5,
    marginLeft: 10,
    marginTop: 10,
    borderColor: "#7A8888",
    borderWidth: 1,
    borderRadius: 8,
  },
  button: {
    backgroundColor: "#96C4E1",
    padding: 15,
    width: "100%",
    borderRadius: 30,
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
  },
});
