module.exports = {
    env: {
        browser: true,
        es2021: true,
        "react-native/react-native": true,
    },
    extends: "plugin:react/recommended",
    overrides: [],
    parser: "@typescript-eslint/parser",
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
        ecmaFeatures: {
            jsx: true,
        },
    },
    plugins: ["react", "@typescript-eslint", "react-native"],
    rules: {
"react/react-in-jsx-scope": "off",
    },

};
