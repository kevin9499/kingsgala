import {Text, TextProps} from './Themed';

export function MonoText(props: TextProps) {
    return <Text {...props} style={[props.style, {fontFamily: 'space-mono'}]}/>;
}

export function Montserrat(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'montserrat'}]} />;
}
export function MontserratBold(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'montserrat-bold'}]} />;
}
export function MontserratSemi(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'montserrat-semi'}]} />;
}
