import { View, Text } from "./Themed";
import { UserContext } from "../contexts/UserContext";
import { useContext } from "react";
import { StyleSheet } from "react-native";

export type AppHeaderProps = {
  subtitle: string;
};

export default function AppHeader(props: AppHeaderProps) {
  const { profile, setProfile } = useContext(UserContext);
  return (
    <View style={styles.titleContainer}>
      <Text style={styles.title}>Hi {profile.firstname}, Let's</Text>
      <Text style={styles.subTitle}>{props.subtitle} </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  titleContainer: {
    flex: 1,
    justifyContent: "flex-start",
    marginTop: "20%",
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
  },
  subTitle: {
    fontSize: 18,
    color: "#FFFFFFCC",
  },
});
