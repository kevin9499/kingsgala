import { Text, View } from "../Themed";
import {
  TextInput,
  StyleSheet,
  TouchableHighlight,
  InputAccessoryView,
} from "react-native";
import { useContext, useState } from "react";
import { login, LoginResponse } from "../../services/AuthService";
import { verifEmail } from "../../helpers/UserHelpers";
import { UserContext } from "../../contexts/UserContext";
import Checkbox from "expo-checkbox";

export type ContactFormProps = {
  onSave: () => void;
};

export default function UserForgetPassword(props: ContactFormProps) {
  const [email, setEmail] = useState("");

  const redir = () => {
    props.onSave();
  };

  return (
    <View>
      <Text style={styles.text}>Let's try to remember</Text>
      <View style={styles.inputLine}>
        <Text style={styles.labelText}>Email</Text>
        <TextInput
          style={styles.textInput}
          value={email}
          onChangeText={setEmail}
        />
      </View>
      <View style={styles.bottom}>
        <TouchableHighlight style={styles.button}>
          <Text style={styles.buttonText} onPress={redir}>
            Reset my password
          </Text>
        </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inputLine: {
    flexDirection: "row",
    margin: 10,
    backgroundColor: "#283636",
    borderRadius: 10,
  },
  labelText: {
    color: "grey",
    margin: 10,
  },
  textInput: {
    width: "50%",
    color: "#fff",
  },
  button: {
    backgroundColor: "#96C4E1",
    padding: 15,
    width: "100%",
    borderRadius: 30,
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
  },
  bottom: {
    alignItems: "center",
    top: 50,
  },
  link: {
    color: "#96C4E1",
  },
  text: {
    bottom: 100,
    textAlign: "center",
    fontSize: 30,
  },
  checkText: {
    marginLeft: 10,
  },
  checkBox: {
    display: "flex",
    flexDirection: "row",
    marginLeft: 30,
  },
  reset: {
    top: 40,
    left: "12%",
  },
});
