import { Text, View } from "../Themed";
import { TextInput, StyleSheet, TouchableHighlight } from "react-native";
import { useContext, useState } from "react";
import { login, getProfile, ProfileResponse } from "../../services/AuthService";
import { verifEmail } from "../../helpers/UserHelpers";
import { UserContext } from "../../contexts/UserContext";
import Checkbox from "expo-checkbox";

export type ContactFormProps = {
  onSave: () => void;
  signUp: () => void;
  reset: () => void;
};

export default function UserFormLogin(props: ContactFormProps) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState(Array<String>());
  const { setTokens } = useContext(UserContext);
  const { setProfile } = useContext(UserContext);
  const [isChecked, setChecked] = useState(false);

  const loginCheck = () => {
    let errorEmail = verifEmail(email);
    let errorsForm = [];
    if (errorEmail != "") errorsForm.push(errorEmail);
    setErrors(errorsForm);
    if (errorsForm.length == 0) {
      login(email, password)
        .then((response: any) => {
          if (response.access_token || response.refresh_token) {
            getProfile(response.access_token).then(
              (response: string | ProfileResponse) => {
                if (typeof response == "string") {
                  console.error(response);
                } else {
                  setProfile(response);
                }
              }
            );
            setTokens(response.access_token, response.refresh_token);
          }
        })
        .catch((err) => console.log(err));
      setEmail("");
      setPassword("");
      props.onSave();
    }
  };
  const redir = () => {
    props.signUp();
  };
  const reset = () => {
    props.reset();
  };

  return (
    <View>
      <Text style={styles.text}>Glad to see you back</Text>
      <View style={styles.inputLine}>
        <TextInput
          style={styles.textInput}
          value={email}
          onChangeText={setEmail}
          keyboardType="email-address"
          placeholder="Email"
          placeholderTextColor={"#7A8888"}
        />
      </View>
      <View style={styles.inputLine}>
        <TextInput
          secureTextEntry={true}
          style={styles.textInput}
          value={password}
          onChangeText={setPassword}
          placeholder="Password"
          placeholderTextColor={"#7A8888"}
        />
      </View>
      {errors.map((item: String, index) => {
        return (
          <Text key={index} lightColor="red" darkColor="red">
            {item}
          </Text>
        );
      })}
      <View style={styles.checkBox}>
        <Checkbox
          value={isChecked}
          onValueChange={setChecked}
          color={isChecked ? "#96C4E1" : undefined}
        />
        <Text style={styles.checkText}>Keep me Sign</Text>
      </View>
      <View style={styles.reset}>
        <Text>
          Forgot password ?{" "}
          <Text style={styles.link} onPress={reset}>
            reset now
          </Text>
        </Text>
      </View>
      <View style={styles.bottom}>
        <TouchableHighlight style={styles.button} onPress={loginCheck}>
          <Text style={styles.buttonText}>Log In</Text>
        </TouchableHighlight>
        <Text>
          Create account ?{" "}
          <Text style={styles.link} onPress={redir}>
            Sign up
          </Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inputLine: {
    flexDirection: "row",
    margin: 10,
    backgroundColor: "#283636",
    borderRadius: 10,
    width: "80%",
  },
  labelText: {
    color: "grey",
    margin: 10,
  },
  textInput: {
    padding: 5,
    marginLeft: 10,
    width: "100%",
    color: "#fff",
  },
  button: {
    backgroundColor: "#96C4E1",
    padding: 15,
    width: "100%",
    borderRadius: 30,
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
  },
  bottom: {
    alignItems: "center",
    bottom: -200,
  },
  link: {
    color: "#96C4E1",
  },
  text: {
    bottom: 100,
    textAlign: "center",
    fontSize: 30,
  },
  checkText: {
    marginLeft: 10,
  },
  checkBox: {
    display: "flex",
    flexDirection: "row",
    marginLeft: 16,
  },
  reset: {
    top: 40,
    left: "12%",
  },
});
