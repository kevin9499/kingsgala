import { Text, View, Button, Header } from "../Themed";
import { TextInput, StyleSheet, TouchableHighlight, Image } from "react-native";
import { useContext, useState } from "react";
import {
  verifEmail,
  verifFirstname,
  verifName,
  verifPassword,
  verifPhone,
} from "../../helpers/UserHelpers";
import { update } from "../../services/AuthService";
import { UserContext } from "../../contexts/UserContext";

export type ContactFormProps = {
  redirect: () => void;
};

export default function UserProfil(props: ContactFormProps) {
  const { profile, setProfile, token, setTokens } = useContext(UserContext);
  const [firstname, setFirstname] = useState(profile.firstname);
  const [lastname, setLastname] = useState(profile.lastname);
  const [email, setEmail] = useState(profile.email);
  const [phone, setPhone] = useState(profile.phone);
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [errors, setErrors] = useState(Array<String>());

  const updateUser = () => {
    let errorName = verifName(lastname),
      errorFirstname = verifFirstname(firstname),
      errorPhone = verifPhone(phone),
      errorEmail = verifEmail(email),
      errorPassword = verifPassword(password, password2);
    let errorsForm = [];
    if (errorName != "") errorsForm.push(errorName);
    if (errorFirstname != "") errorsForm.push(errorFirstname);
    if (errorPhone != "") errorsForm.push(errorPhone);
    if (errorEmail != "") errorsForm.push(errorEmail);
    if (errorEmail != "") errorsForm.push(errorPassword);
    setErrors(errorsForm);
    if (errorsForm.length == 0) {
      update(profile.id, firstname, lastname, phone, email, password)
        .then((value) => console.log(value))
        .catch((err) => console.log(err));
      let id: number = profile.id;
      setProfile({ email, firstname, lastname, id, phone });
      setPassword("");
      setPassword2("");
      props.redirect();
    }
  };

  const logOut = () => {
    setTokens(null,null)
    props.redirect
  }

  return (
    <View style={{ width: "100%" }}>
      <View style={{ paddingLeft: 30, paddingRight: 30 }}>
        <Text style={styles.text}>{firstname}</Text>
        <Text style={styles.text}>{lastname}</Text>
      </View>
      <View style={styles.input}>
        <View style={styles.inputLine}>
          <TextInput
            style={styles.textInput}
            value={firstname}
            onChangeText={setFirstname}
            placeholder="First Name"
            placeholderTextColor={"#7A8888"}
          />
        </View>
        <View style={styles.inputLine}>
          <TextInput
            style={styles.textInput}
            value={lastname}
            onChangeText={setLastname}
            placeholder="Last Name"
            placeholderTextColor={"#7A8888"}
          />
        </View>
        <View style={styles.inputLine}>
          <TextInput
            style={styles.textInput}
            value={email}
            onChangeText={setEmail}
            keyboardType="email-address"
            placeholder="Email"
            placeholderTextColor={"#7A8888"}
          />
        </View>
        <View style={styles.inputLine}>
          <TextInput
            style={styles.textInput}
            value={profile.phone}
            onChangeText={setPhone}
            keyboardType="phone-pad"
            placeholder="Phone Number"
            placeholderTextColor={"#7A8888"}
          />
        </View>
      </View>
      <View style={styles.inputPw}>
        <View style={styles.inputLine}>
          <TextInput
            secureTextEntry={true}
            style={styles.textInput}
            value={password}
            onChangeText={setPassword}
            placeholder="Password"
            placeholderTextColor={"#7A8888"}
          />
        </View>
        <View style={styles.inputLine}>
          <TextInput
            secureTextEntry={true}
            style={styles.textInput}
            value={password2}
            onChangeText={setPassword2}
            placeholder="Confirm Password"
            placeholderTextColor={"#7A8888"}
          />
        </View>
      </View>
      {errors.map((item: String, index) => {
        return (
          <Text key={index} lightColor="red" darkColor="red">
            {item}
          </Text>
        );
      })}
      <View style={styles.bottom}>
        <TouchableHighlight style={styles.button} onPress={updateUser}>
          <Text style={styles.buttonText}>Save</Text>
        </TouchableHighlight>
      </View>
      <View style={styles.bottom}>
        <TouchableHighlight style={styles.button2} onPress={logOut}>
          <Text style={styles.buttonText}>Log out</Text>
        </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inputLine: {
    flexDirection: "row",
    margin: 8,
    backgroundColor: "#283636",
    borderRadius: 10,
    width: "80%",
  },
  labelText: {
    color: "grey",
    margin: 10,
  },
  textInput: {
    padding: 5,
    marginLeft: 10,
    color: "#fff",
    width: "100%",
  },
  button: {
    backgroundColor: "#96C4E1",
    padding: 10,
    width: "100%",
    borderRadius: 30,
  },
  button2: {
    backgroundColor: "#b42758",
    padding: 10,
    top:20,
    width: "100%",
    borderRadius: 30,
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
  },
  inputPw: {
    top: 20,
    width: "100%",
    marginTop: "10%",
    paddingLeft: 30,
    paddingRight: 30,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  link: {
    color: "#96C4E1",
  },
  bottom: {
    alignItems: "center",
    bottom: -40,
    paddingLeft: 30,
    paddingRight: 30,
  },
  text: {
    bottom: 50,
    textAlign: "left",
    fontSize: 28,
  },
  input: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    top: 20,
    width: "100%",
    paddingLeft: 30,
    paddingRight: 30,
  },
});
