import { Text, View, Button, Header } from "../Themed";
import { TextInput, StyleSheet, TouchableHighlight } from "react-native";
import { useContext, useState } from "react";
import {
  verifEmail,
  verifFirstname,
  verifName,
  verifPassword,
  verifPhone,
} from "../../helpers/UserHelpers";
import { register } from "../../services/AuthService";

export type ContactFormProps = {
  redirect: () => void;
};

export default function userFormRegister(props: ContactFormProps) {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [type, setType] = useState(2);
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [errors, setErrors] = useState(Array<String>());

  const saveContact = () => {
    let errorName = verifName(lastname),
      errorFirstname = verifFirstname(firstname),
      errorPhone = verifPhone(phone),
      errorEmail = verifEmail(email),
      errorPassword = verifPassword(password, password2);
    let errorsForm = [];
    if (errorName != "") errorsForm.push(errorName);
    if (errorFirstname != "") errorsForm.push(errorFirstname);
    if (errorPhone != "") errorsForm.push(errorPhone);
    if (errorEmail != "") errorsForm.push(errorEmail);
    if (errorEmail != "") errorsForm.push(errorPassword);
    setErrors(errorsForm);
    if (errorsForm.length == 0) {
      register(firstname, lastname, phone, email, password)
        .then((value) => console.log(value))
        .catch((err) => console.log(err));
      setFirstname("");
      setEmail("");
      setPhone("");
      setLastname("");
      setPassword("");
      setPassword2("");
      props.redirect();
    }
  };

  return (
    <View>
      <View>
        <Text style={styles.text}>Welcome</Text>
      </View>
      <View style={styles.inputLine}>
        <TextInput
          style={styles.textInput}
          value={firstname}
          placeholder="Firstname"
          placeholderTextColor={"#7A8888"}
          onChangeText={setFirstname}
        />
      </View>
      <View style={styles.inputLine}>
        <TextInput
          style={styles.textInput}
          value={lastname}
          placeholder="Lastname"
          placeholderTextColor={"#7A8888"}
          onChangeText={setLastname}
        />
      </View>
      <View style={styles.inputLine}>
        <TextInput
          style={styles.textInput}
          value={email}
          onChangeText={setEmail}
          placeholder="Email"
          placeholderTextColor={"#7A8888"}
          keyboardType="email-address"
        />
      </View>
      <View style={styles.inputLine}>
        <TextInput
          style={styles.textInput}
          value={phone}
          onChangeText={setPhone}
          placeholder="Phone Number"
          placeholderTextColor={"#7A8888"}
          keyboardType="phone-pad"
        />
      </View>
      <View style={styles.inputPw}>
        <View style={styles.inputLine}>
          <TextInput
            secureTextEntry={true}
            style={styles.textInput}
            value={password}
            onChangeText={setPassword}
            placeholder="Password"
            placeholderTextColor={"#7A8888"}
          />
        </View>
        <View style={styles.inputLine}>
          <TextInput
            secureTextEntry={true}
            style={styles.textInput}
            value={password2}
            onChangeText={setPassword2}
            placeholder="Confirm Password"
            placeholderTextColor={"#7A8888"}
          />
        </View>
      </View>
      {errors.map((item: String, index) => {
        return (
          <Text key={index} lightColor="red" darkColor="red">
            {item}
          </Text>
        );
      })}
      <View style={styles.bottom}>
        <TouchableHighlight style={styles.button} onPress={saveContact}>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableHighlight>
        <Text style={{ marginTop: 20 }}>
          Already have an account ?{" "}
          <Text style={styles.link} onPress={props.redirect}>
            Log In
          </Text>
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inputLine: {
    margin: 10,
    backgroundColor: "#283636",
    borderRadius: 10,
  },
  labelText: {
    color: "grey",
    margin: 10,
  },
  textInput: {
    color: "#fff",
    padding: 5,
    marginLeft: 10,
  },
  button: {
    backgroundColor: "#96C4E1",
    padding: 15,
    width: "100%",
    borderRadius: 30,
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
  },
  inputPw: {
    marginTop: "10%",
  },
  link: {
    color: "#96C4E1",
  },
  bottom: {
    alignItems: "center",
    bottom: -30,
  },
  text: {
    bottom: 30,
    textAlign: "center",
    fontSize: 30,
  },
});
