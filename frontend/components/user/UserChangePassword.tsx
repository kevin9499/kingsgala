import { Text, View } from "../Themed";
import {
  TextInput,
  StyleSheet,
  TouchableHighlight,
  InputAccessoryView,
} from "react-native";
import { useContext, useState } from "react";
import { login, LoginResponse } from "../../services/AuthService";
import { verifEmail } from "../../helpers/UserHelpers";
import { UserContext } from "../../contexts/UserContext";
import Checkbox from "expo-checkbox";

export type ContactFormProps = {
  onSave: () => void;
};

export default function UserChangePassword(props: ContactFormProps) {
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");

  const redir = () => {
    props.onSave();
  };

  return (
    <View>
      <Text style={styles.text}>We found you !</Text>
      <Text style={styles.text2}>You can change your password</Text>
      <View style={styles.inputPw}>
        <View style={styles.inputLine}>
          <Text style={styles.labelText}>Password:</Text>
          <TextInput
            secureTextEntry={true}
            style={styles.textInput}
            value={password}
            onChangeText={setPassword}
          />
        </View>
        <View style={styles.inputLine}>
          <Text style={styles.labelText}>Confirm password:</Text>
          <TextInput
            secureTextEntry={true}
            style={styles.textInput}
            value={password2}
            onChangeText={setPassword2}
          />
        </View>
      </View>
      <View style={styles.bottom}>
        <TouchableHighlight style={styles.button}>
          <Text style={styles.buttonText} onPress={redir}>
            Change password
          </Text>
        </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inputLine: {
    flexDirection: "row",
    margin: 10,
    backgroundColor: "#283636",
    borderRadius: 10,
  },
  labelText: {
    color: "grey",
    margin: 10,
  },
  textInput: {
    width: "50%",
    color: "#fff",
  },
  button: {
    backgroundColor: "#96C4E1",
    padding: 15,
    width: "100%",
    borderRadius: 30,
  },
  buttonText: {
    color: "black",
    textAlign: "center",
    fontSize: 20,
  },
  bottom: {
    alignItems: "center",
    top: 50,
  },
  link: {
    color: "#96C4E1",
  },
  text: {
    bottom: 100,
    textAlign: "center",
    fontSize: 30,
  },
  text2: {
    bottom: 100,
    textAlign: "center",
    fontSize: 20,
  },
  checkText: {
    marginLeft: 10,
  },
  checkBox: {
    display: "flex",
    flexDirection: "row",
    marginLeft: 30,
  },
  reset: {
    top: 40,
    left: "12%",
  },
  inputPw: {
    marginTop: "10%",
  },
});
