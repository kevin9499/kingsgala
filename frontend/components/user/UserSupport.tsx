import {Text, View, Button, Header} from "../Themed";
import {TextInput, StyleSheet, TouchableHighlight, Image} from "react-native";
import {useContext, useState} from "react";
import {UserContext} from "../../contexts/UserContext";
import {MaterialCommunityIcons} from "@expo/vector-icons";

export type ContactFormProps = {
    back: () => void;
};

export default function UserSupport(props: ContactFormProps) {

    return (
        <View style={styles.text}>
            <Text></Text>
        </View>
    );
}
const styles = StyleSheet.create({
    text: {
        flex: 3,
        textAlign: "center",
        alignItems: "center"
    },
    text2: {
        color: "grey",
        
        right: 35
    },
    box: {
        textAlign: "center",
        alignItems: "center",
        width: 300,
        borderRadius: 20,
        paddingTop: 5,
        paddingBottom: 5,
    },
    img: {
        left: 100,
        top: 30
    },
    line: {
        borderColor: "black"
    }, separator: {
        marginVertical: 30,
        height: 1,
        width: "80%",
    },
});
