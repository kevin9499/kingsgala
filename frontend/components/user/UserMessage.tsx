import { Text, View, Button, Header } from "../Themed";
import { TextInput, StyleSheet, TouchableHighlight, Image } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export type ContactFormProps = {
  redirect: () => void;
};

export default function UserMessage(props: ContactFormProps) {
  const goToMessage = () => {
    props.redirect();
  };

  return (
    <View style={styles.text}>
      <View style={styles.box}>
        <TouchableHighlight>
          <MaterialCommunityIcons
            style={styles.img}
            name={"message-text"}
            color={"#96C4E1"}
            size={30}
          />
        </TouchableHighlight>
        <Text style={styles.text2}>26 - octobre</Text>
        <Text>Chambord - Wedding </Text>
        <View
          style={styles.separator}
          lightColor="#eee"
          darkColor="rgba(255,255,255,0.1)"
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  text: {
    flex: 3,
    textAlign: "center",
    alignItems: "center",
  },
  text2: {
    color: "grey",
    right: 35,
  },
  box: {
    textAlign: "center",
    alignItems: "center",
    width: 300,
    borderRadius: 20,
    paddingTop: 5,
    paddingBottom: 5,
  },
  img: {
    left: 100,
    top: 30,
  },
  line: {
    borderColor: "black",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
