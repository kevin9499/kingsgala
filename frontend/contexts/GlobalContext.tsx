import React from 'react';
import {UserContextProvider} from "./UserContext";

export class GlobalContextProvider extends React.Component<{ children?: React.ReactNode; }, {}> {

    render() {
        return (
            <UserContextProvider>
                {this.props.children}
            </UserContextProvider>
        );
    }
}