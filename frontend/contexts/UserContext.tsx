import React from "react";
import data from "../assets/jsons/cart.json";
import { saveValue } from "../services/StorageService";
import { ProfileResponse } from "../services/AuthService";

// Declaring the state object globally.
type UserStateType = {
  token: string | null;
  refreshToken: string | null;
  profile: ProfileResponse;
};
type CastleStateType = {
  castle: {
    name: string;
    image: string;
    service: string;
    start: string;
    end: string; // Declaring the state object globally.
    price: // Declaring the state object globally.
    number;
  }[];
};

const initialUserState: UserStateType = {
  token: "",
  refreshToken: "",
  profile: {} as ProfileResponse,
};

const initialCastleState: CastleStateType = {
  castle: [],
};

const userContextWrapper = (component?: React.Component) => ({
  ...initialUserState,
  ...initialCastleState,
  setTokens: (token: any, refreshToken: any) => {
    initialUserState.token = token;
    initialUserState.refreshToken = refreshToken;
    saveValue("token", token);
    saveValue("refeshToken", refreshToken);
    component?.setState({ context: userContextWrapper(component) });
  },
  setProfile: (profile: ProfileResponse) => {
    initialUserState.profile = profile;
    saveValue("profile", JSON.stringify(profile));
    component?.setState({ context: userContextWrapper(component) });
  },
  removeTokens: () => {
    initialUserState.token = "";
    initialUserState.refreshToken = "";
    component?.setState({ context: userContextWrapper(component) });
  },
  loadCastle: () => {
    initialCastleState.castle = data;
    component?.setState({ context: userContextWrapper(component) });
  },
});

type Context = ReturnType<typeof userContextWrapper>;

export const UserContext = React.createContext<Context>(userContextWrapper());

interface State {
  context: Context;
}

export class UserContextProvider extends React.Component<
  { children?: React.ReactNode },
  {}
> {
  state: State = {
    context: userContextWrapper(this),
  };

  render() {
    return (
      <UserContext.Provider value={this.state.context}>
        {this.props.children}
      </UserContext.Provider>
    );
  }
}
