import AsyncStorage from "@react-native-async-storage/async-storage";

export async function readValue(key: string) {
    return await AsyncStorage.getItem(key);
}

export async function saveValue(key: string, value: string) {
    await AsyncStorage.setItem(key, value);
}