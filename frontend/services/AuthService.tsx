import axios from "axios";
// @ts-ignore
//mport { URL } from "@env";
const URL = "http://192.168.1.27:3000"
export type LoginResponse = {
  access_token: string | null;
  refresh_token: string | null;
};

export type ProfileResponse = {
  email: string;
  firstname: string;
  lastname: string;
  id: number;
  phone: string;
};

export async function getProfile(access: string) {
  try {
    const { data } = await axios.get<ProfileResponse>(`${URL}/profile`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${access}`,
      },
    });
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.error("error message: ", error.message);
      return error.message;
    } else {
      console.log("unexpected error: ", error);
      return "An unexpected error occurred";
    }
  }
}

export async function login(email: string, password: string) {
  try {
    // 👇️ const data: LoginResponse
    const { data } = await axios.post<LoginResponse>(
      URL + "/auth/login",
      { username: email, password: password },
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );

    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log("error message: ", error.message);
      // 👇️ error: AxiosError<any, any>
      return error.message;
    } else {
      console.log("unexpected error: ", error);
      return "An unexpected error occurred";
    }
  }
}

export async function register(
  firstname: string,
  lastname: string,
  phone: string,
  email: string,
  password: string
) {
  try {
    // 👇️ const data: LoginResponse
    const { data } = await axios.post(
      URL + "/users",
      {
        firstname: firstname,
        lastname: lastname,
        email: email,
        type: 2,
        phone: phone,
        password: password,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );

    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log("error message: ", error.message);
      // 👇️ error: AxiosError<any, any>
      return error.message;
    } else {
      console.log("unexpected error: ", error);
      return "An unexpected error occurred";
    }
  }
}

export async function update(
  id: number,
  firstname: string,
  lastname: string,
  phone: string,
  email: string,
  password: string
) {
  try {
    // 👇️ const data: LoginResponse
    const { data } = await axios.put(
      URL + "/users",
      {
        id: id,
        firstname: firstname,
        lastname: lastname,
        email: email,
        type: 2,
        phone: phone,
        password: password,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );

    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log("error message: ", error.message);
      // 👇️ error: AxiosError<any, any>
      return error.message;
    } else {
      console.log("unexpected error: ", error);
      return "An unexpected error occurred";
    }
  }
}

export async function refreshToken(tokens: LoginResponse) {
  try {
    // 👇️ const data: LoginResponse
    const { data } = await axios.post<LoginResponse>(
      URL + "/auth/refreshtoken",
      tokens,
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    );
    return data;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log("error message: ", error.message);
      // 👇️ error: AxiosError<any, any>
      return error.message;
    } else {
      console.log("unexpected error: ", error);
      return "An unexpected error occurred";
    }
  }
}
