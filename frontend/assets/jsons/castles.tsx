export const castles = [
  {
    name: "Chambord",
    image: require("../images/first_card.jpg"),
    description: "One of the most awe-inspiring Renaissance constructions",
    price: 300,
    map: require("../images/cham_carte.png"),
    phone: "1111111111",
  },
  {
    name: "Chenonceau",
    image: require("../images/second_card.jpg"),
    description: "One of the most awe-inspiring Renaissance constructions",
    price: 250,
    map: require("../images/cheno_carte.png"),
    phone: "2222222222",
  },
  {
    name: "Versaille",
    image: require("../images/third_card.jpg"),
    description: "One of the most awe-inspiring Renaissance constructions",
    price: 1000,
    map: require("../images/versa_carte.png"),
    phone: "3333333333",
  },
  {
    name: "Mont Saint-Michel",
    image: require("../images/fourth_card.jpg"),
    description: "One of the most awe-inspiring Renaissance constructions",
    price: 480,
    map: require("../images/michel_carte.png"),
    phone: "4444444444",
  },
];
