const tintColorLight = "#2f95dc";
const tintColorDark = "#96C4E1";

export default {
  light: {
    text: "#fff",
    background: "#191D21",
    tint: tintColorDark,
    tabIconDefault: "#FFFFFFCC",
    tabIconSelected: tintColorDark,
  },
  dark: {
    text: "#fff",
    background: "#191D21",
    tint: tintColorDark,
    tabIconDefault: "#FFFFFFCC",
    tabIconSelected: tintColorDark,
  },
};
