<div align="center">
  <img src="./resources/assets/logo.svg" height="128" />
  <br />
  <h1>KINGSGALA</h1>
  <blockquote>
  <p>React Native application to book castles for amazing events</p>
  </blockquote>
  <br />
  <img src="https://forthebadge.com/images/badges/built-with-love.svg" />
  <br />
  <img src="https://github.com/Pixidream/kingsgala/actions/workflows/lint.yml/badge.svg?branch=main" />
</div>

---
## Annexes
Figma (wireframes): https://www.figma.com/file/0ZJpoGMQ7JEbAiVIyUS9Fo/Kingsgala?node-id=49%3A93  
Canva (diapo): https://www.canva.com/design/DAEvEM0qUEI/yqcz2zQHqraNBrKFHht3Rw/view?utm_content=DAEvEM0qUEI&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink

## Requirements

- docker (with docker-compose)
- node 16

## Development

### Environnement

dans `frontend/.env` ajouter la variable `URL=http://<ipv4 du pc>:3000`

## Database

Lancer le docker-compose du backend:  
`cd backend && docker compose up -d` (docker-compose sur les version plus anciennes de docker)

une fois le compose lancé, il va falloir sur rendre dans le container mysql pour ajouter les usertype a la main:  
`docker compose exec -it db /bin/bash`

ensuite, faire un `mysql -p` (mot de passe = 123)

enfin:

```sql
USE kingsgala;

INSERT typeuser VALUES (1, "admin");
INSERT typeuser VALUES (2, "user");
```

## Lancer les serveurs

A cette etape, le backend est deja lancé, mais si jamais:  
`cd backend && docker compose up -d`

pour le font:
installer les packages: `npm i`  
lancer expo: `npm start`

tout devrait mtn etre bon et fonctionner.
il ne reste plus qu'a se creer un compte sur l'appli et
check que tout marche.

si jamais il manque une etape, je suis joingnable par telephone.

## Stopper les serveurs et clean le images

`docker compose down --volumes --remove-orphans`
